angular.module('starter.controllers', [])

    .controller('LoginCtrl', function ($scope, LoginService, $ionicPopup, $state) {
        $scope.data = {};
        $scope.loginstor = function () {
            LoginService.loginStorage($scope).success(function () {
                $state.go('tab.account')
            }).error(function () {
                $state.go('login')
            })
        };
        $scope.login = function () {
            LoginService.loginUser($scope.data.username, $scope.data.password).success(function (data) {
                $state.go('tab.account');
            }).error(function (data) {
                var alertPopup = $ionicPopup.alert({
                    title: 'Login failed!',
                    template: 'Please check your credentials!'
                });
            });
        };
        $scope.logout = function () {
            LoginService.loginOut($state).success(function () {
                $state.go('login');
            });
        }
    })

    .controller('AccountCtrl', function ($scope, RaceFactory, UsersServiceIn) {
        $scope.user = JSON.parse(window.localStorage.getItem('user'));
        $scope.groups = {
            1: {name: 'Отдел продаж'},
            2: {name: 'Отдел обслуживания'},
            3: {name: 'Отдел гос. закупок'}
        };
        $scope.position = UsersServiceIn.getPosition($scope.user)
        $scope.cars = RaceFactory;
    })


    .controller('RaceInCtrl', function ($scope, RaceFactory, UsersInFactory, UsersServiceIn) {
        $scope.user = JSON.parse(window.localStorage.getItem('user'));
        $scope.users = UsersInFactory;
        $scope.cars = RaceFactory;
        $scope.position = UsersServiceIn.getPosition($scope.user)
})
    .controller('RaceTeamCtrl', function ($scope, RaceFactory) {
        $scope.user_dep = JSON.parse(window.localStorage.getItem('user')).group
        $scope.bus = {1:'img/bus/grey.png', 2:'img/bus/blue.png', 3:'img/bus/red.png'};
        $scope.dep = {1: {name: 'Отдел продаж', position:2},
            2: {name: 'Отдел обслуживания', position:1},
            3: {name: 'Отдел гос. закупок', position:3}};
    })
    .controller('RaceOutCtrl', function ($scope, RaceFactory, UsersOutFactory, UsersServiceOut) {
        $scope.user = JSON.parse(window.localStorage.getItem('user'));
        $scope.users = UsersOutFactory;
        $scope.cars = RaceFactory;
        $scope.position = UsersServiceOut.getPosition($scope.user)
    })


