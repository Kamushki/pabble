angular.module('starter.services', [])
    .service('LoginService', function ($q, $http) {
        return {
            loginOut: function ($state) {
                var deferred = $q.defer();
                var promise = deferred.promise;
                window.localStorage.clear();
                promise.success = function (fn) {
                    promise.then(fn);
                    $state.go('login');
                    return promise;
                }
                promise.error = function (fn) {
                    promise.then(null, fn);
                    return promise;
                }
                return promise;
            },
            loginStorage: function () {
                var deferred = $q.defer();
                var promise = deferred.promise;
                if (window.localStorage.getItem('user') != null) {
                    deferred.resolve('Welcome!');
                }
                promise.success = function (fn) {
                    promise.then(fn);
                    return promise;
                }
                promise.error = function (fn) {
                    promise.then(null, fn);
                    return promise;
                }
                return promise;
            },
            loginUser: function (email, password) {
                var deferred = $q.defer();
                var promise = deferred.promise;
                var formData = 'email=' + email + '&password=' + password;
                $http({
                    method: 'post',
                    url: 'http://dev1.wilto.ru/auth',
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                    data: formData
                }).success(function (res) {
                    deferred.resolve('Welcome!');
                    //set user in local storage
                    // window.localStorage.setItem('auth_hash',res.auth_hash);
                    window.localStorage.setItem('user', JSON.stringify(res));
                }).error(function (error) {
                    deferred.reject('Wrong credentials.');
                    console.log(error);
                });
                promise.success = function (fn) {
                    promise.then(fn);
                    return promise;
                }
                promise.error = function (fn) {
                    promise.then(null, fn);
                    return promise;
                }
                return promise;
            }
        }
    })
    .service('UsersServiceIn', function ($http) {
        return {
            getPosition: function (user) {
                var obj = {content: 0};
                $http({
                    method: "get",
                    url: "http://dev1.wilto.ru/users/"+user.group
                }).success(function (data) {
                    data.result = data.result.sort(function (a, b) {
                        return a.trans_count - b.trans_count;
                    });
                    for (var i = 0; i < data.result.length; i++) {
                        if (user.id == data.result[i].id) {
                            obj.content = i+1
                        }
                    }
                })
                    return obj;
            }
        }

    })

    .service('UsersServiceOut', function ($http) {
        return {
            getPosition: function (user) {
                var obj = {content: []};
                var arr = {content: null};
                var ret = {content: 0};
                $http({
                    method: "get",
                    url: "http://dev1.wilto.ru/users/"+1
                }).success(function (data) {
                    arr.content = data
                    var x = arr.content.result.sort(function (a, b) {
                        return a.trans_count - b.trans_count
                    });
                    for( var i = 0; i < x.length; i++) {
                        obj.content.push(x[i]);
                    }
                });
                $http({
                    method: "get",
                    url: "http://dev1.wilto.ru/users/"+2
                }).success(function (data) {
                    arr.content = data
                    var x = arr.content.result.sort(function (a, b) {
                        return a.trans_count - b.trans_count
                    });
                    for( var i = 0; i < x.length; i++) {
                        obj.content.push(x[i]);
                    }
                });
                $http({
                    method: "get",
                    url: "http://dev1.wilto.ru/users/"+3
                }).success(function (data) {
                    arr.content = data
                    var x = arr.content.result.sort(function (a, b) {
                        return a.trans_count - b.trans_count
                    });
                    for (var i = 0; i < x.length; i++) {
                        obj.content.push(x[i]);
                    }
                    for (var i = 0; i < obj.content.length; i++) {
                        if (user.id == obj.content[i].id) {
                            ret.content = i + 1
                            break;
                        }
                    }
                })

                return ret;
            }
        }
    })
    .factory('UsersInFactory', function ($http) {
        var obj = {content: null};
        var group = JSON.parse(window.localStorage.getItem('user')).group
        $http({
            method: "get",
            url: "http://dev1.wilto.ru/users/"+group
        }).success(function (data) {
            obj.content = data;
            obj.content = obj.content.result.sort(function (a, b) {
                return a.trans_count - b.trans_count;
            });
        });
        return obj;
    })
    .factory('UsersOutFactory', function ($http) {
        var obj = {content: []};
        var arr = {content: null};
        $http({
            method: "get",
            url: "http://dev1.wilto.ru/users/"+1
        }).success(function (data) {
            arr.content = data
            var x = arr.content.result.sort(function (a, b) {
                return a.trans_count - b.trans_count
            });
            for( var i = 0; i < x.length; i++) {
                obj.content.push(x[i]);
            }
        });
        $http({
            method: "get",
            url: "http://dev1.wilto.ru/users/"+2
        }).success(function (data) {
            arr.content = data
            var x = arr.content.result.sort(function (a, b) {
                return a.trans_count - b.trans_count
            });
            for( var i = 0; i < x.length; i++) {
                obj.content.push(x[i]);
            }
        });
        $http({
            method: "get",
            url: "http://dev1.wilto.ru/users/"+3
        }).success(function (data) {
            arr.content = data
            var x = arr.content.result.sort(function (a, b) {
                return a.trans_count - b.trans_count
            });
            for( var i = 0; i < x.length; i++) {
                obj.content.push(x[i]);
            }
        });
        console.log(obj.content)
        return obj;
    })

    .factory('RaceFactory', function () {
        var cars = {
            1: {type: 'легковая', color: 'синий', price: 1000, img: 'img/cars/blue_car.png'},
            2: {type: 'легковая', color: 'красный', price: 1500, img: 'img/cars/red_car.png'},
            3: {type: 'легковая', color: 'оранжевый', price: 1500, img: 'img/cars/orange_car.png'},
            4: {type: 'легковая', color: 'зеленый', price: 1000, img: 'img/cars/green_car.png'},
            5: {type: 'легковая', color: 'желтый', price: 1000, img: 'img/cars/yellow_car.png'}
        }
        return cars;
    })
